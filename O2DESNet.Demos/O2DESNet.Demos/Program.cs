﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace O2DESNet.Demos
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("1.\tG/G/n Queue System");
            Console.WriteLine("2.\tTwo-Restore-Server System");
            Console.WriteLine("------------------------------");
            while (true)
            {
                try
                {
                    Console.Write("Choose the example to run: ");
                    switch (Convert.ToInt32(Console.ReadLine()))
                    {
                        case 1: GGnQueue.Program.Main(); break;
                        case 2: TwoRestoreServer.Program.Main(); break;
                    }
                }
                catch
                {
                    Console.WriteLine("Input is not valid...");
                }
            }
        }
    }
}
